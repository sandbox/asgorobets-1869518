-- SUMMARY --

Menu Slice can be very handy module in some cases. It's purpose is to
ease content editor's job and provide powerful tools to work with menus.

You can use Menu Slice if you need to:
* Create menus from subtrees of existing menus.
* Clone menu subtrees to an existing menu.

Take a look at Menu Block module if you need to display a subtree in a block:
http://drupal.org/project/menu_block

* Use Menu Block when you want to display a subtree of your menu in a block.
* Use Menu Slice when you want to separate a subtree into a different menu.
You will like it in cases when you have a big navigation menu with thousands
of links and you need to create an OG Menu for your organic group with just
one subtree of the big menu.

Check out the Demo:
http://www.youtube.com/watch?v=lA7ufB6eD3E


-- REQUIREMENTS --

Menu module is required to use Menu Slice module.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.


-- CONFIGURATION --

* Configure user permissions in Administration » People » Permissions:

  Add "Administer Menu Slice" permission for user roles that will have access to
  module's configuration page.

  Note: Dealing with menus affects your database entries, make sure to have
  recent backups and do your testings not on production environment.

* Access Menu Slice administration page in
  Administration » Structure » Menus » Menu Slice.

-- HOW TO USE --

1. Go to Menu Slice administration page on
Administration » Structure » Menus » Menu Slice.
2. Choose a parent item from "Parent item" select box.
Desired parent item with it's children will be cloned.
3. Choose existing menu from "Select existing menu" select box.
Links will be created in this menu.
 - or -
Check "Create new menu" checkbox. Specify menu title.
Menu machine name should be generated according to new menu title.
You can specify your own machine-name by clicking "Edit".
4. Click "Submit"
5. You will be redirected to the menu configuration page.
Please provide some description if necessary and check out
your new links on "List Links" tab.

-----

Feel free to report bugs and suggest new features in the issue queue
